FROM node:10.15-alpine

ENV HOME=/home/app

COPY package.json package-lock.json $HOME/

WORKDIR $HOME

RUN npm install --silent --progress=false

COPY . $HOME/

RUN npm run build

CMD ["npm", "start"]

EXPOSE 3000