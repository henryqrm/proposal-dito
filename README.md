# Dito Timeline test

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Antes de começar

Para liberar aquela dopamina, quebrei nas seguintes tasks:

- Entender os requisitos do teste
- Separar responsabilidades, conceitos e tecnologia
- Montar a arquitetura
- Componentizar HTML e CSS
- Fazer a integração com API

## O Projeto

Como proposto pelo teste, essa Single Page Application escrita em React 16.8\* segue os requisitos enviado pela Dito. Construi esse projeto por meio de um fork do projeto create-react-app versão typescript, na qual, adicionei algumas funcionalidades, têcnicas e ferramentas para auxiliar no desenvolvimento, com foco na qualidade, manutenção e experiência do desenvolvedor ao prosseguir com o desenvolvimento e manutenção do projeto. Os itens adicionados foram:

- git hooks: Ao fazer um commit, é executado scripts de testes, formatter e linters dos arquivos modificados na branch/staged através do husky. Evitando código sujo.
- typing SCSS Module: Intellisense para CSS Modules
- linters: Para manter a uniformidade e estilo de código de acordo com code style para typescript e SASS, TSlint e stylelint respectivamente.
- formatter: Ao commitar, formata automaticamente os arquivos alterados através do prettier.
- alias path: evita path hell ../../../../myLib.ts
- bundle analyzer: UI de verificação do tamanho das dependencias e dos modulos, tanto em gzip, quanto tamanho real. O que é extremamente útil para analise, decisões de performance, libs de terceiro e se poderá haver uma melhor modularização.
- setup de test: Cypress para teste e2e, TDD/BDD, etc

Busco escrever HTML semântico recomendado pela especificação [W3C](https://www.w3.org/), e CSS utilizando o preprocessador [SASS](http://sass-lang.com/) com [CSS Modules](https://github.com/css-modules/css-modules) através do guide [RSCSS](http://rscss.io/) que tem como premissa a componentização e performance. Para códificar, uso Typescript para tipagem e abstrações, seguindo claro o paradgma funcional devido a suas vantagens sob OO, e auxiliado pelo style-guide airbnb com modificações. Nesta aplicação não utilizei nenhum framework CSS. Por se tratar de um pequeno teste para avaliação, e por eu estar um pouco sem tempo, o projeto contem falhas e coisas que podem ser melhoradas como:

- não esta responsivo para os mobiles, pensei em Desktop-first.
- não é PWA
- não me empenhei para estruturar melhor o store atráves de Context API/hooks ou até mesmo redux.
- é um monorepo, o ideal seria usar um gerênciador de repositório tipo o [lerna](https://github.com/lerna/lerna) e separar melhor em pacotes
- poderia ter granularizado os componentes de UI, tornando o diálogo com os desigers mais concisos de um possível Design System ou [_atomic design_](http://bradfrost.com/blog/post/atomic-web-design/) com o intuito de tornar ainda mais fiés ao design. Assim como a [Atlassian](https://bitbucket.org/atlassian/atlaskit-mk-2/src/master/) faz.
- e por fim, não teve testes. Perdi uns pontos aqui! Eu sei! Quando estou meio sem tempo infelizmente vou na marretada! Tu sabe como é. É foda. :/

Assim sendo, as principais pastas são:

1. **/components** - Componentes que não possuem estado, mutações ou ações com a Context API/Redux.
2. **/containers** - Blocos de components que possuem complexidade isolada, com mutações e ações explicitas.
3. **/routes** - Contém a rota com a visualização final, compõe e conecta conteiners
4. **/store** - fonte única de dados, estrutura mutável compartilhada pelos containers, podendo ter mais de um contexto.

## O test da Dito

O HTML e CSS foi bem tranquilo. Apesar de não ter uma ferramenta de comunicação como zeplin/figma busquei ser pixel perfect, ressaltando que os svgs de place e money que foi exportado no tamanho errado. Então ficou a cargo da integração com o endpoint o que foi o desafio maior.

Para tal, quebrei em dois problemas, o primeiro agrupar por `transaction_id`, que através do conceito de recursão agrupei em uma nova estrutura de dados em que o index é a `transaction_id`. Em segundo, parsear para o modelo que define para a timeline através de uma função redutora.

É relevênte falar que deste que venho estudando mais programação funcional não uso `else`, `try catch`, `switch` e nem `let` no meu código. E cada dia mais sem o `if`. O que torna o código ainda mais légivel e descritivo!

## Executando o projeto

### docker

```sh
npm run docker:up
```

ou

```sh
docker build -t henryqrm/dito-timeline .
docker run --rm -p 3000:3000 henryqrm/dito-timeline
```

### Makefile

<sub>necessário ter versão lst [NodeJS](https://nodejs.org/en/) ou superior e NPM</sub>

```sh
make install
make build
make start
```

### npm

```sh
npm i
npm run build
npm start
```

Se você leu até aqui! Valeu demais! Fiz na correria da semana de prova + trabalho da pós graduação, trabalho CLTesão, Avengers e GOT sem spoilers! #VaiDarBão
Dúvidas, no email [henryqrm@gmail.com](mailto:henryqrm@gmail.com) (Henrique Rodrigues)
