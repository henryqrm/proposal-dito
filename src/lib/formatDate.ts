export default (date: string) => {
  if (!date) {
    return '--'
  }
  const newDate = new Date(date)
  const year = newDate.getFullYear()
  const month: number | string = newDate.getMonth() + 1
  const day: number | string = newDate.getDate()

  return `${day < 10 ? `0${day}` : day}/${
    month < 10 ? `0${month}` : month
  }/${year}`
}
