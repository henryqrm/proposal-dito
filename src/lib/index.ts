export { default as to } from './to'

export const not = (value: any) => !value
