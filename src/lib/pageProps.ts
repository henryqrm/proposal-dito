import { RouteConfigComponentProps } from 'react-router-config'

export interface PageProps<Params = {}>
  extends RouteConfigComponentProps<Params> {}
