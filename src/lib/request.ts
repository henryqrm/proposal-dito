import to from './to'

export type Methods = 'GET'

export const request = async <T>(url: string, method: Methods) => {
  const config: RequestInit = {
    method,
    credentials: 'same-origin',
    headers: {
      'Content-Type': 'application/json',
      Accept: 'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    },
  }
  const [results, err] = await to(fetch(url, config))

  if (err) {
    throw err
  }

  return results.json() as T
}

export const get = <T>(url: string) => request<T>(url, 'GET')
