export default (date: string) => {
  if (!date) {
    return '00:00'
  }
  const _date = new Date(date)
  return `${_date.getHours()}:${_date.getMinutes()}`
}
