// TODO: Terminar de tipar
const to = async <T>(toResolve): Promise<[T, undefined]> =>
  toResolve.then(res => [res as T, undefined]).catch(err => [undefined, err])

export default to
