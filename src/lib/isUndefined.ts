export default (value: unknown) => typeof value === 'undefined'
