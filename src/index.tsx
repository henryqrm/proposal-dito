import React from 'react'
import { render } from 'react-dom'
import * as serviceWorker from './serviceWorker'
import Bootstrap from './containers/Bootstrap'
import '@assets/styles/index.scss'

const rootEl = document.getElementById('root')
// const renderMethod = module.hot ? render : hydrate

render(<Bootstrap />, rootEl)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()

module.hot &&
  module.hot.accept('./containers/Bootstrap', () => {
    render(<Bootstrap />, rootEl)
  })
