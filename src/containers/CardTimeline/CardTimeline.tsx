import React, { useState } from 'react'
import styles from './CardTimeline.module.scss'
import Card from '@components/Card/Card'
import Icon from '@components/Icon/Icon'
import classNames from 'classnames'
import { CardModel } from '@store/timeline'

export interface CardTimelineProps extends CardModel {
  multiplyAnimation: number
}

export default ({ multiplyAnimation, ...card }: CardTimelineProps) => {
  const [effect, setEffect] = useState<boolean>(false)
  const timerEffect = multiplyAnimation * 100
  const doEffect = () => setEffect(true)
  const cardClassName = classNames({
    [styles.card]: true,
    [styles.cardShow]: effect,
  })

  setTimeout(doEffect, timerEffect)

  return (
    <div className={styles.wrapper}>
      <div className={styles.icon}>
        <Icon type="check" size="md" />
      </div>
      <div className={cardClassName}>
        <Card {...card} />
      </div>
    </div>
  )
}
