import React from 'react'
import styles from './Timeline.module.scss'
import { useTimeline } from '@store/timeline'
import CardTimeline from '@containers/CardTimeline/CardTimeline'

export default () => {
  const [cards, isLoading, isError, doLoadMock] = useTimeline()

  const MockComponenet = () => (
    <div style={{ marginLeft: 50 }}>
      <button
        style={{
          border: 0,
          background: 'inherit',
          cursor: 'pointer',
          color: 'blue',
        }}
        onClick={doLoadMock}
      >
        Load mock
      </button>
    </div>
  )

  return (
    <div className={styles.timeline}>
      <div className={styles.cards}>
        {isError ? (
          <MockComponenet />
        ) : (
          cards.map((card, index) => (
            <CardTimeline key={index} multiplyAnimation={index + 1} {...card} />
          ))
        )}
      </div>
    </div>
  )
}
