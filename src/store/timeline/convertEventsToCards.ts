import isUndefined from '@lib/isUndefined'
import isEqual from '@lib/isEqual'
import { CardModel } from './models'
import formatDate from '@lib/formatDate'
import formatHour from '@lib/formatHour'
import {
  PURCHASED_PRODUCT,
  PURCHASED,
  PRODUCT_NAME,
  TRANSACTION_ID,
  PRODUCT_PRICE,
  STORE_NAME,
  TransitionId,
  EmptyObject,
} from './types'

const makeMoney = (value: string | number) => `R$ ${value},00`

interface CustomData {
  key: PRODUCT_NAME | TRANSACTION_ID | PRODUCT_PRICE | STORE_NAME
  value: number | string
}

export interface Transition {
  event: PURCHASED_PRODUCT | PURCHASED
  timestamp: string
  revenue?: number
  custom_data: CustomData[]
}

const hasTransactionId = (nextEvent, transactionId) =>
  nextEvent.hasOwnProperty(transactionId)

const mergeWithNewTransitionId = (
  transactionId: TransitionId,
  event: Transition,
  nextEvent: EventsGroup | EmptyObject,
) => ({
  ...{
    [transactionId]: [event],
  },
  ...nextEvent,
})

interface EventsGroup {
  [key: string]: Transition[]
}

const mergeWithSameTransitionId = (
  transactionId: TransitionId,
  event: Transition,
  nextEvent: EventsGroup | EmptyObject,
) => ({
  ...nextEvent,
  [transactionId]: [event, ...nextEvent[transactionId]],
})

const findTransitionId = (customData: CustomData[]) =>
  customData.find(data => isEqual(data.key, 'transaction_id'))

const getTransactionId = (customData: CustomData[]): TransitionId => {
  const customDataCurrentTransitionId = findTransitionId(customData)

  return customDataCurrentTransitionId
    ? (customDataCurrentTransitionId.value as TransitionId)
    : ''
}

const hasTransactionIdValue = (transactionId: string) => transactionId !== ''

const mergeByTransactionId = (event: Transition, othersEvent: Transition[]) => {
  const transactionId = getTransactionId(event.custom_data)

  return hasTransactionIdValue(transactionId)
    ? mergeByTransitionId(transactionId, event, othersEvent)
    : {}
}

const groupEventsByTransactionId = ([event, ...othersEvent]: Transition[]):
  | EventsGroup
  | EmptyObject =>
  isUndefined(event) ? {} : mergeByTransactionId(event, othersEvent)

const mergeByTransitionId = (
  transactionId: TransitionId,
  event: Transition,
  othersEvent: Transition[],
) => {
  const nextEvent = groupEventsByTransactionId(othersEvent)

  return hasTransactionId(nextEvent, transactionId)
    ? mergeWithSameTransitionId(transactionId, event, nextEvent)
    : mergeWithNewTransitionId(transactionId, event, nextEvent)
}

const getRevenue = (event: Transition) =>
  event.revenue ? makeMoney(event.revenue) : 'R$ 0,00'

const getLocale = (customData: CustomData[]) => {
  const data = customData.find(_data => isEqual(_data.key, 'store_name'))
  return (data ? data.value : '--') as string
}
const getProductName = (customData: CustomData[]) => {
  const data = customData.find(_data => isEqual(_data.key, 'product_name'))
  return (data ? data.value : '--') as string
}
const getProductPrice = (customData: CustomData[]) => {
  const data = customData.find(_data => isEqual(_data.key, 'product_price'))
  return data ? makeMoney(data.value) : 'R$ 0,00'
}

const makePurchases = (card: CardModel, transition: Transition) => ({
  ...card,
  revenue: getRevenue(transition),
  date: formatDate(transition.timestamp),
  hour: formatHour(transition.timestamp),
  locale: getLocale(transition.custom_data),
})

const makePurchasesProduct = (card: CardModel, transition: Transition) => ({
  ...card,
  purchases: [
    {
      price: getProductPrice(transition.custom_data),
      product: getProductName(transition.custom_data),
    },
    ...card.purchases,
  ],
})

const reducerCard = (card: CardModel, previousTransition: Transition) => {
  if (isEqual(previousTransition.event, 'comprou')) {
    return makePurchases(card, previousTransition)
  }

  if (isEqual(previousTransition.event, 'comprou-produto')) {
    return makePurchasesProduct(card, previousTransition)
  }

  return card
}

const mapperGroupsToCard = (eventsGroup: EventsGroup) => (
  transitionId: TransitionId,
) =>
  eventsGroup[transitionId].reduce(reducerCard, {
    purchases: [],
  } as any) as CardModel

const parserTimeline = (eventsGroup: EventsGroup) =>
  Object.keys(eventsGroup).map(mapperGroupsToCard(eventsGroup))

export default (events: Transition[]) => {
  const group = groupEventsByTransactionId(events)
  return parserTimeline(group)
}
