export interface PurchaseModel {
  product: string
  price: string
}

export interface CardModel {
  date: string
  hour: string
  locale: string
  revenue: string
  purchases: PurchaseModel[]
}
