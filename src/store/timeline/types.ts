export type PURCHASED_PRODUCT = 'comprou-produto'
export type PURCHASED = 'comprou'
export type PRODUCT_NAME = 'product_name'
export type TRANSACTION_ID = 'transaction_id'
export type PRODUCT_PRICE = 'product_price'
export type STORE_NAME = 'store_name'
export type TransitionId = string
// tslint:disable-next-line
export type EmptyObject = {}
