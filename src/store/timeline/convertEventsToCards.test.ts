/// <reference types="Cypress" />

import convertEventsToCards from './convertEventsToCards'
import mock from './mock'

describe('Store: Timeline', () => {
  it('convertEventsToCards', () => {
    cy.wrap(convertEventsToCards(mock.events)).snapshot()
  })
})
