import { useState, useEffect } from 'react'
import { CardModel } from './models'
import { BASE_URL } from '@/constants'
import convertEventsToCards, { Transition } from './convertEventsToCards'
import isUndefined from '@lib/isUndefined'
import mock from './mock'

const API_EVENTS = '/events.json'
import { request } from '@lib/request'
import to from '@lib/to'

interface DitoEvents {
  events: Transition[]
}

export default (): [CardModel[], boolean, boolean, () => void] => {
  const [cards, setCards] = useState<CardModel[]>([])
  const [isLoading, setLoading] = useState(true)
  const [isError, setError] = useState(false)
  const fetchUrl = async () => {
    const [result, err] = await to<DitoEvents>(
      request(BASE_URL.concat(API_EVENTS), 'GET'),
    )

    if (err || isUndefined(result)) {
      setError(true)
      return
    }

    const _cards = convertEventsToCards((result || {}).events || [])

    setCards(_cards)
    setLoading(false)
  }

  const didUpdate = () => {
    fetchUrl()
  }

  useEffect(didUpdate, [])

  const loadMock = () => {
    setCards(convertEventsToCards((mock || {}).events || []))
    setError(false)
    setLoading(false)
  }

  return [cards, isLoading, isError, loadMock]
}
