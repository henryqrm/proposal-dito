import React from 'react'
import styles from './HomePage.module.scss'
import TimeLine from '@containers/Timeline/TimeLine'

export default () => (
  <div className={styles.home}>
    <TimeLine />
  </div>
)
