import { RouteConfig } from 'react-router-config'
import Home from './Home'
import NotFound from './NotFound'

const routes: RouteConfig[] = [Home, NotFound]

export default routes
