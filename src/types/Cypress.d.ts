declare namespace Cypress {
  interface Chainable<Subject> {
    snapshot: typeof snapshot
  }
}

function snapshot(): void {}

Cypress.Commands.add('snapshot', snapshot)
