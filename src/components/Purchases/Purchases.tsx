import React from 'react'
import styles from './Purchases.module.scss'
import { PurchaseModel } from '@store/timeline'

export interface PurchasesProps {
  purchases: PurchaseModel[]
}

export default ({ purchases }: PurchasesProps) => {
  const Header = () => (
    <thead>
      <tr>
        <th>Produto</th>
        <th>Preço</th>
      </tr>
    </thead>
  )

  const Row = ({ product, price }: PurchaseModel) => (
    <tr>
      <td children={product} />
      <td children={price} />
    </tr>
  )

  const Body = () => (
    <tbody>
      {purchases.map((purchase, index) => (
        <Row key={index} {...purchase} />
      ))}
    </tbody>
  )

  return (
    <table className={styles.table}>
      <Header />
      <Body />
    </table>
  )
}
