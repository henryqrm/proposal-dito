import React from 'react'
import styles from './Label.module.scss'
export interface LabelProps {
  text: string
}

export default ({ text }: LabelProps) => (
  <span className={styles.label} children={text} />
)
