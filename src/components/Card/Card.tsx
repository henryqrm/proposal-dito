import React from 'react'
import { CardModel } from '@store/timeline'
import Purchases from '../Purchases/Purchases'
import LabelWitnIcon, {
  LabelWitnIconProps,
} from '../LabelWitnIcon/LabelWitnIcon'
import styles from './Card.module.scss'

export default ({ revenue, date, hour, locale, purchases }: CardModel) => {
  const listLabelsProps: LabelWitnIconProps[] = [
    {
      type: 'calendar',
      text: date,
    },
    {
      type: 'clock',
      text: hour,
    },
    {
      type: 'place',
      text: locale,
    },
    {
      type: 'money',
      text: revenue,
    },
  ]

  const Header = () => (
    <header className={styles.header}>
      <ul>
        {listLabelsProps.map((itemProp, index) => (
          <li key={index}>
            <LabelWitnIcon {...itemProp} key={index} />
          </li>
        ))}
      </ul>
    </header>
  )

  const Body = () => (
    <article className={styles.body}>
      <Purchases purchases={purchases} />
    </article>
  )

  return (
    <div className={styles.card}>
      <Header />
      <Body />
    </div>
  )
}
