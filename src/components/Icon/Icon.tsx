import React from 'react'
import calendar from '@assets/icons/calendar.svg'
import clock from '@assets/icons/clock.svg'
import place from '@assets/icons/place.svg'
import money from '@assets/icons/money.svg'
import check from '@assets/icons/check.svg'
import layout from '@assets/images/layout.png'
import classNames from 'classnames'
import styles from './Icon.module.scss'
import isEqual from '@lib/isEqual'

export type IconTypes = 'calendar' | 'clock' | 'check' | 'place' | 'money'
export type SizeSM = 'sm'
export type SizeMD = 'md'
export type IconSize = SizeSM | SizeMD

export interface IconProps {
  type: IconTypes
  size?: IconSize
}

export const iconTypes = {
  calendar,
  clock,
  place,
  money,
  layout,
  check,
}

export const SM: SizeSM = 'sm'
export const MD: SizeMD = 'md'

export default ({ type, size = SM }: IconProps) => (
  <span
    className={classNames({
      [styles.icon]: true,
      [styles.iconSm]: isEqual(size, SM),
      [styles.iconMd]: isEqual(size, MD),
    })}
    style={{ backgroundImage: `url('${iconTypes[type]}')` }}
  />
)
