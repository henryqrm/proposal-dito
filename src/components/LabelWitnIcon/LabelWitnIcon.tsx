import React from 'react'
import Icon, { IconTypes } from '../Icon/Icon'
import Label from '../Label/Label'
import styles from './LabelWitnIcon.module.scss'

export interface LabelWitnIconProps {
  text: string
  type: IconTypes
}

export default ({ text, type }: LabelWitnIconProps) => (
  <div className={styles.labelWithIcon}>
    <Icon type={type} />
    <Label text={text} />
  </div>
)
